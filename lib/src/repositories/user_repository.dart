import 'package:firebase_auth/firebase_auth.dart';
import 'package:moview_flutter/src/models/favorite.dart';
import 'package:moview_flutter/src/models/user.dart';
import 'package:moview_flutter/src/resources/firestore_user_provider.dart';

class UserRepository {
  FirestoreUserProvider _firestoreUserProvider = FirestoreUserProvider();

  Future<bool> isUserConnected() => _firestoreUserProvider.isUserConnected();

  Future<void> registerUser({User newUser, String password}) =>
      _firestoreUserProvider.registerUser(newUser: newUser, password: password);

  Future<void> signOutUser() => _firestoreUserProvider.signOutUser();

  Future<FirebaseUser> signInUser({String email, String password}) =>
      _firestoreUserProvider.signInUser(email: email, password: password);

  Future<bool> isEmailFree({String email}) =>
      _firestoreUserProvider.isEmailFree(email: email);

  Future<void> addOrRemoveFavorite({Favorite favorite}) =>
      _firestoreUserProvider.addOrRemoveFavorite(favorite: favorite);

  Future<List<Favorite>> getAllFavorite() =>
      _firestoreUserProvider.getAllFavorite();
}
