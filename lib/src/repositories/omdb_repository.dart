import 'package:moview_flutter/src/models/movie.dart';
import 'package:moview_flutter/src/models/search_results.dart';
import 'package:moview_flutter/src/models/series.dart';
import 'package:moview_flutter/src/resources/omdb_api_provider.dart';

class OMDBRepository {
  final omdbApiProvider = OMDBApiProvider();

  Future<SearchResults> searchItemsByName(
          {String searchedName, String searchType}) =>
      omdbApiProvider.searchItemsByName(
          searchedName: searchedName, searchType: searchType);

  Future<Movie> searchMovieById({String id}) =>
      omdbApiProvider.searchMovieById(id: id);

  Future<Series> getSeriesById({String id}) =>
      omdbApiProvider.getSeriesById(id: id);
}
