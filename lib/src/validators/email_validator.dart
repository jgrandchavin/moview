import 'dart:async';
import 'package:moview_flutter/src/repositories/user_repository.dart';

const String _kEmailRule = r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
UserRepository userRepository = UserRepository();

class EmailValidator {
  final StreamTransformer<String, String> validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    final RegExp emailExp = new RegExp(_kEmailRule);

    if (!emailExp.hasMatch(email) || email.isEmpty) {
      sink.addError('No valid email.');
    } else {
      sink.add(email);
    }
  });
}

class EmailFreeValidator {
  final StreamTransformer<String, String> isEmailFree =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (email, sink) async {
    userRepository.isEmailFree(email: email);
    sink.addError('Loading...');
    if (await userRepository.isEmailFree(email: email)) {
      sink.add(email);
    } else {
      sink.addError('Email already taken.');
    }
  });
}
