import 'dart:async';

class CommonTextValidator {
  final StreamTransformer<String, String> validateText =
      StreamTransformer<String, String>.fromHandlers(handleData: (text, sink) {
    if (text.length == 0) {
      sink.addError('Do not let this field empty.');
    } else {
      sink.add(text);
    }
  });
}
