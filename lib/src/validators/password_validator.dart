import 'dart:async';

class PasswordValidator {
  final StreamTransformer<String, String> validatePassword =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (password, sink) {
    if (password.length < 5) {
      sink.addError('Password too short');
    } else {
      sink.add(password);
    }
  });
}
