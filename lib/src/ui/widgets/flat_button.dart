import 'package:flutter/material.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';

class CommonFlatButton extends StatelessWidget {
  final bool isValid;
  final bool isLoading;
  final Function function;
  final String text;

  CommonFlatButton({this.isValid, this.isLoading, this.function, this.text});

  @override
  Widget build(BuildContext context) {
    print(isValid);
    return FlatButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      color: ColorsConstants.red,
      textColor: Colors.white,
      disabledColor: ColorsConstants.lightGrey,
      onPressed: isValid ? function : null,
      child: Container(
          height: 40,
          child: Center(
              child: isLoading ? CircularProgressIndicator() : Text(text))),
    );
  }
}
