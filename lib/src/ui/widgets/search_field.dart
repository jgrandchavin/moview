import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';

class SearchField extends StatelessWidget {
  final TextEditingController textEditingController;
  final Function onChanged;

  SearchField({@required this.textEditingController, @required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
          border: Border.all(color: ColorsConstants.lightGrey),
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Row(
        children: <Widget>[
          SvgPicture.asset('assets/glasses.svg'),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Search...',
                  hintStyle: TextStyle(
                      color: ColorsConstants.lightGrey,
                      fontWeight: FontWeight.w200)),
              controller: textEditingController,
              onChanged: (value) => onChanged(value),
            ),
          ),
        ],
      ),
    );
  }
}
