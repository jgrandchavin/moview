import 'package:flutter/material.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';

class CommonTextFiled extends StatelessWidget {
  final String hintText;
  final String errorText;
  final Function onChange;

  CommonTextFiled({this.hintText, this.errorText, this.onChange});

  @override
  Widget build(BuildContext context) {
    return TextField(
        onChanged: onChange,
        decoration: InputDecoration(
            hintText: hintText,
            errorText: errorText,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: ColorsConstants.lightGrey))));
  }
}
