import 'package:flutter/material.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';

class HomePage extends StatelessWidget {
  final Widget child;

  HomePage({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsConstants.red,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'm',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontFamily: 'Mechsuit', color: Colors.white, fontSize: 50),
          ),
          Text(
            'Welcome to Moview!',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 30),
          ),
        ],
      ),
    );
  }
}
