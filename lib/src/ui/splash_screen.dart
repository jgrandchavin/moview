import 'package:flutter/material.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsConstants.red,
      body: Center(
        child: Text(
          'm',
          style: TextStyle(
              fontFamily: 'Mechsuit', color: Colors.white, fontSize: 50),
        ),
      ),
    );
  }
}
