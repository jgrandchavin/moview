import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moview_flutter/src/blocs/navigation/application_navigation_bloc.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ApplicationNavigationBloc>(
      creator: (BuildContext context, BlocCreatorBag bag) {
        return ApplicationNavigationBloc();
      },
      child: CurrentView(),
    );
  }
}

class CurrentView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ApplicationNavigationBloc applicationNavigationBloc =
        BlocProvider.of<ApplicationNavigationBloc>(context);

    return Scaffold(
      bottomNavigationBar: BottomNavbar(
        applicationNavigationBloc: applicationNavigationBloc,
      ),
      body: StreamBuilder<Widget>(
        stream: applicationNavigationBloc.outView,
        builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          if (snapshot.hasData) {
            return snapshot.data;
          }
          return Container();
        },
      ),
    );
  }
}

class BottomNavbar extends StatelessWidget {
  final ApplicationNavigationBloc applicationNavigationBloc;

  BottomNavbar({Key key, this.applicationNavigationBloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
        stream: applicationNavigationBloc.outIndex,
        initialData: 0,
        builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
          return BottomNavigationBar(
            onTap: (int index) {
              applicationNavigationBloc.changeIndex(index);
            },
            currentIndex: snapshot.data,
            items: [
              _navBarItem(
                  icon: 'assets/home.svg',
                  title: 'Home',
                  isActive: snapshot.data == 0),
              _navBarItem(
                  icon: 'assets/play-button.svg',
                  title: 'Search',
                  isActive: snapshot.data == 1),
              _navBarItem(
                  icon: 'assets/pin.svg',
                  title: 'Map',
                  isActive: snapshot.data == 2),
              _navBarItem(
                  icon: 'assets/account.svg',
                  title: 'Account',
                  isActive: snapshot.data == 3),
            ],
          );
        });
  }

  BottomNavigationBarItem _navBarItem(
      {String icon, String title, bool isActive}) {
    return BottomNavigationBarItem(
      icon: Container(
          padding: EdgeInsets.only(top: 10),
          child: SvgPicture.asset(
            icon,
            color: isActive ? ColorsConstants.red : ColorsConstants.black,
          )),
      title: Container(
        padding: EdgeInsets.only(top: 5),
        child: Text(
          title,
          style: TextStyle(
              color: isActive ? ColorsConstants.red : ColorsConstants.black,
              fontWeight: FontWeight.w200),
        ),
      ),
    );
  }
}
