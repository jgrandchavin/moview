import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/authentication/login_bloc.dart';
import 'package:moview_flutter/src/blocs/authentication/register_bloc.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';
import 'package:moview_flutter/src/ui/authentication/login_page.dart';
import 'package:moview_flutter/src/ui/widgets/flat_button.dart';
import 'package:moview_flutter/src/ui/widgets/text_field.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Bloc
    final RegisterBloc registerBloc = BlocProvider.of<RegisterBloc>(context);

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        body: Container(
          child: ListView(
            physics: ClampingScrollPhysics(),
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 300,
                decoration: BoxDecoration(color: ColorsConstants.red),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(
                      height: 50,
                    ),
                    Text(
                      'Welcome!',
                      style: TextStyle(
                          color: ColorsConstants.red,
                          fontSize: 26,
                          fontWeight: FontWeight.w600),
                    ),
                    Text(
                      'We need some information!',
                      style: TextStyle(
                          color: ColorsConstants.red,
                          fontSize: 20,
                          fontWeight: FontWeight.w200),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    StreamBuilder<String>(
                      stream: registerBloc.outEmail,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        return CommonTextFiled(
                          errorText: snapshot.error,
                          onChange: registerBloc.inEmail,
                          hintText: 'Email address',
                        );
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    StreamBuilder<String>(
                      stream: registerBloc.outPassword,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        return CommonTextFiled(
                          errorText: snapshot.error,
                          onChange: registerBloc.inPassword,
                          hintText: 'Password',
                        );
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    StreamBuilder<String>(
                      stream: registerBloc.outConfirmPassword,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        return CommonTextFiled(
                          errorText: snapshot.error,
                          onChange: registerBloc.inConfirmPassword,
                          hintText: 'Confirm Password',
                        );
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    StreamBuilder<bool>(
                        stream: registerBloc.isRegisterFormValid,
                        initialData: false,
                        builder: (context, snapshot) {
                          return StreamBuilder<bool>(
                              stream: registerBloc.outIsLoading,
                              initialData: false,
                              builder: (context, snapshot2) {
                                return CommonFlatButton(
                                  function: () =>
                                      registerBloc.registerUser(context),
                                  isLoading: snapshot2.data,
                                  isValid: (snapshot.hasData && snapshot.data),
                                  text: 'Register',
                                );
                              });
                        }),
                    SizedBox(
                      height: 20,
                    ),
                    CommonFlatButton(
                      text: 'You have an account ? Log in',
                      function: () {
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return BlocProvider<LoginBloc>(
                              creator: (_context, _bag) => LoginBloc(),
                              child: LoginPage());
                        }), (Route<dynamic> route) => false);
                      },
                      isLoading: false,
                      isValid: true,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
