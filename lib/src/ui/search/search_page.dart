import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/navigation/search_navigation_bloc.dart';
import 'package:moview_flutter/src/blocs/search_movie_bloc.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';
import 'package:moview_flutter/src/ui/search/pages/movies_list_page.dart';

class SearchPage extends StatelessWidget {
  final Widget child;

  SearchPage({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SearchNavigationBloc>(
      child: Content(),
      creator: (BuildContext context, BlocCreatorBag bag) =>
          SearchNavigationBloc(),
    );
  }
}

class Content extends StatelessWidget {
  Content({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final SearchNavigationBloc searchNavigationBloc =
        BlocProvider.of<SearchNavigationBloc>(context);

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        appBar: SearchPageAppBar(
          searchNavigationBloc: searchNavigationBloc,
        ),
        body: StreamBuilder<Widget>(
          stream: searchNavigationBloc.outView,
          initialData: BlocProvider<SearchMovieBloc>(
            child: MoviesListPage(),
            creator: (BuildContext context, BlocCreatorBag bag) =>
                SearchMovieBloc(),
          ),
          builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
            return snapshot.data;
          },
        ),
      ),
    );
  }
}

class SearchPageAppBar extends StatelessWidget implements PreferredSizeWidget {
  final SearchNavigationBloc searchNavigationBloc;

  SearchPageAppBar({Key key, @required this.searchNavigationBloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new SizedBox.fromSize(
        size: preferredSize,
        child: StreamBuilder<String>(
            stream: searchNavigationBloc.outViewKey,
            initialData: 'movies',
            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
              return Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                        child: InkWell(
                            onTap: () {
                              searchNavigationBloc.changeView('movies');
                            },
                            child: Container(
                              height: 60,
                              child: Container(
                                padding: EdgeInsets.only(top: 15),
                                child: Text(
                                  'Movies',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontWeight: snapshot.data == 'movies'
                                          ? FontWeight.w500
                                          : FontWeight.w100),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              decoration: BoxDecoration(
                                  border: Border(
                                top: BorderSide(
                                    color: snapshot.data == 'movies'
                                        ? Colors.white
                                        : Colors.transparent,
                                    width: 2),
                              )),
                            ))),
                    Expanded(
                        child: InkWell(
                            onTap: () {
                              searchNavigationBloc.changeView('series');
                            },
                            child: Container(
                              height: 60,
                              child: Container(
                                padding: EdgeInsets.only(top: 15),
                                child: Text(
                                  'Series',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontWeight: snapshot.data == 'series'
                                          ? FontWeight.w500
                                          : FontWeight.w200),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              decoration: BoxDecoration(
                                  border: Border(
                                top: BorderSide(
                                    color: snapshot.data == 'series'
                                        ? Colors.white
                                        : Colors.transparent,
                                    width: 2),
                              )),
                            ))),
                  ],
                ),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: ColorsConstants.shadow.withOpacity(.6),
                          blurRadius: 20,
                          spreadRadius: 0)
                    ],
                    color: ColorsConstants.red,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
              );
            }));
  }

  @override
  Size get preferredSize => Size(0, 145);
}
