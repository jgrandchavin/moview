import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moview_flutter/src/blocs/search_movie_bloc.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';
import 'package:moview_flutter/src/models/search_item_card.dart';
import 'package:moview_flutter/src/models/search_results.dart';
import 'package:moview_flutter/src/ui/details/movie_detail.dart';
import 'package:moview_flutter/src/ui/widgets/search_field.dart';

class MoviesListPage extends StatelessWidget {
  MoviesListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _textEditingController = TextEditingController();
    final SearchMovieBloc _moviesBloc =
        BlocProvider.of<SearchMovieBloc>(context);

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 40),
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: SearchField(
            textEditingController: _textEditingController,
            onChanged: (String value) {
              _moviesBloc.inSearchedTitle.add(value);
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
        StreamBuilder<SearchResults>(
          stream: _moviesBloc.outItemsList,
          initialData: SearchResults(results: [], totalResults: -1),
          builder:
              (BuildContext context, AsyncSnapshot<SearchResults> snapshot) {
            if (snapshot.hasError) return Text('${snapshot.error}');
            if (snapshot.hasData) {
              if (snapshot.data.totalResults == -1) {
                return Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Column(
                    children: <Widget>[
                      SvgPicture.asset(
                        'assets/search.svg',
                        fit: BoxFit.cover,
                        height: 150,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        'Let search some movies!',
                        style:
                            TextStyle(color: ColorsConstants.red, fontSize: 15),
                      )
                    ],
                  ),
                );
              } else if (snapshot.data.totalResults == 0) {
                return Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Column(
                    children: <Widget>[
                      SvgPicture.asset(
                        'assets/no-result.svg',
                        fit: BoxFit.cover,
                        height: 150,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        'No results...',
                        style:
                            TextStyle(color: ColorsConstants.red, fontSize: 15),
                      )
                    ],
                  ),
                );
              } else {
                return Expanded(
                  child: ListView(
                      padding: EdgeInsets.only(top: 10, left: 16, right: 16),
                      children: <Widget>[
                        Wrap(
                            runSpacing: 20,
                            spacing: 16,
                            children: snapshot.data.results
                                .map((SearchItemCard movieCard) {
                              return InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return MovieDetail(
                                      movieId: movieCard.id,
                                      item: movieCard,
                                    );
                                  }));
                                },
                                child: MovieCardWidget(
                                  movieCard: movieCard,
                                ),
                              );
                            }).toList())
                      ]),
                );
              }
            }
          },
        )
      ],
    );
  }
}

class MovieCardWidget extends StatelessWidget {
  final SearchItemCard movieCard;

  MovieCardWidget({Key key, @required this.movieCard}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData _mediaQuery = MediaQuery.of(context);
    return Container(
      width: _mediaQuery.size.width / 2 - 24,
      height: 300,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: ColorsConstants.shadow.withOpacity(.3), blurRadius: 10)
          ],
          borderRadius: BorderRadius.all(Radius.circular(22)),
          image: DecorationImage(
              image: NetworkImage(movieCard.posterUrl), fit: BoxFit.cover)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
                color: ColorsConstants.black,
                border: Border.all(color: ColorsConstants.red, width: 0.5),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                      color: ColorsConstants.shadow,
                      blurRadius: 30,
                      spreadRadius: 5)
                ]),
            child: Text(
              '${movieCard.title}',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
