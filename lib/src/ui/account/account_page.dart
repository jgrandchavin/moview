import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/authentication/register_bloc.dart';
import 'package:moview_flutter/src/blocs/favorite_bloc.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';
import 'package:moview_flutter/src/models/favorite.dart';
import 'package:moview_flutter/src/repositories/user_repository.dart';
import 'package:moview_flutter/src/ui/authentication/register_page.dart';
import 'package:moview_flutter/src/ui/details/movie_detail.dart';
import 'package:moview_flutter/src/ui/details/serie_detail.dart';
import 'package:moview_flutter/src/ui/widgets/flat_button.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final UserRepository _userRepository = UserRepository();
    final FavoriteBloc favoriteBloc = BlocProvider.of<FavoriteBloc>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorsConstants.red,
        title: Text('Account'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Text('Favorite', style: TextStyle(fontSize: 20)),
            StreamBuilder<List<Favorite>>(
              stream: favoriteBloc.outItemsList,
              builder: (BuildContext context,
                  AsyncSnapshot<List<Favorite>> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  case ConnectionState.active:
                    return Expanded(
                      child: ListView.builder(
                        padding: EdgeInsets.only(top: 20),
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: InkWell(
                              onTap: () {
                                if (snapshot.data[index].type == 'movie') {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return MovieDetail(
                                      item: snapshot.data[index].searchItemCard,
                                      movieId: snapshot
                                          .data[index].searchItemCard.id,
                                    );
                                  }));
                                } else {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return SerieDetail(
                                      item: snapshot.data[index].searchItemCard,
                                      seriesId: snapshot
                                          .data[index].searchItemCard.id,
                                    );
                                  }));
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                decoration: BoxDecoration(
                                    color: ColorsConstants.red,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(7))),
                                child: Text(
                                  snapshot.data[index].searchItemCard.title,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  case ConnectionState.done:
                }
              },
            ),
            CommonFlatButton(
              function: () {
                _userRepository.signOutUser();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (BuildContext context) {
                  return BlocProvider<RegisterBloc>(
                      creator: (_context, _bag) => RegisterBloc(),
                      child: RegisterPage());
                }), (Route<dynamic> route) => false);
              },
              isValid: true,
              isLoading: false,
              text: 'Log out',
            )
          ],
        ),
      ),
    );
  }
}
