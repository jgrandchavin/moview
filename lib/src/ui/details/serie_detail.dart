import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';
import 'package:moview_flutter/src/models/favorite.dart';
import 'package:moview_flutter/src/models/search_item_card.dart';
import 'package:moview_flutter/src/models/series.dart';
import 'package:moview_flutter/src/repositories/omdb_repository.dart';
import 'package:moview_flutter/src/repositories/user_repository.dart';
import 'package:moview_flutter/src/ui/details/season_detail.dart';

class SerieDetail extends StatelessWidget {
  final String seriesId;
  final SearchItemCard item;

  SerieDetail({this.seriesId, this.item});

  final OMDBRepository omdbRepository = OMDBRepository();
  final UserRepository userRepository = UserRepository();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder<Series>(
          future: omdbRepository.getSeriesById(id: seriesId),
          builder: (BuildContext context, AsyncSnapshot<Series> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
              case ConnectionState.done:
                if (snapshot.hasError)
                  return SafeArea(child: Text('Error: ${snapshot.error}'));
                if (snapshot.hasData) {
                  Series series = snapshot.data;
                  return ListView(
                    physics: ClampingScrollPhysics(),
                    padding: EdgeInsets.only(bottom: 30),
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.5,
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: ColorsConstants.shadow
                                              .withOpacity(.3),
                                          blurRadius:
                                              MediaQuery.of(context).size.width)
                                    ],
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(
                                            MediaQuery.of(context).size.width /
                                                2.5),
                                        bottomRight: Radius.circular(
                                            MediaQuery.of(context).size.width /
                                                2.5)),
                                    image: DecorationImage(
                                        image: NetworkImage(series.posterUrl),
                                        fit: BoxFit.fitWidth)),
                              ),
                              Positioned(
                                top: 10 + MediaQuery.of(context).padding.top,
                                left: 6,
                                child: IconButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  icon: SvgPicture.asset(
                                    'assets/return.svg',
                                    alignment: Alignment(0, 0),
                                    height: 30,
                                    width: 30,
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 10 + MediaQuery.of(context).padding.top,
                                right: 6,
                                child: IconButton(
                                  onPressed: () async {
                                    await userRepository.addOrRemoveFavorite(
                                        favorite: Favorite(
                                            type: 'series',
                                            searchItemCard: item));

                                    SnackBar snackBar = SnackBar(
                                      content:
                                          Text('Favorite successfully added'),
                                    );
                                    Scaffold.of(context).showSnackBar(snackBar);
                                  },
                                  icon: SvgPicture.asset(
                                    'assets/heart.svg',
                                    alignment: Alignment(0, 0),
                                    height: 30,
                                    width: 30,
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text(
                                  series.title,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: ColorsConstants.red,
                                      fontSize: 23,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  '${series.released} - ${series.production}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SvgPicture.asset('assets/star.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      '${series.rating}/100 (${series.voteNumber} votes)',
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/hour.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      series.runtime,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/eye.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      series.rated,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/cup.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      series.awards,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/folder.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      series.genre,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Text(
                                  series.plot,
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            height: 100,
                            child: ListView.builder(
                              itemCount: series.actors.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                          child: SvgPicture.asset(
                                              'assets/person.svg')),
                                      Text(series.actors[index])
                                    ],
                                  ),
                                );
                              },
                              scrollDirection: Axis.horizontal,
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      series.seasons != null
                          ? Column(
                              children: series.seasons.map((Season season) {
                              return FlatButton(
                                color: ColorsConstants.red,
                                onPressed: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return SeasonDetail(
                                      season: season,
                                    );
                                  }));
                                },
                                child: Container(
                                  child: Text(
                                    'Season ${season.number.toString()}',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              );
                            }).toList())
                          : Text(
                              'No seasons information given',
                              textAlign: TextAlign.center,
                            ),
                    ],
                  );
                }
                return Container();
            }
          },
        ),
      ),
    );
  }
}
