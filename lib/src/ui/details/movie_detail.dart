import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';
import 'package:moview_flutter/src/models/favorite.dart';
import 'package:moview_flutter/src/models/movie.dart';
import 'package:moview_flutter/src/models/search_item_card.dart';
import 'package:moview_flutter/src/repositories/omdb_repository.dart';
import 'package:moview_flutter/src/repositories/user_repository.dart';

class MovieDetail extends StatelessWidget {
  final String movieId;
  final SearchItemCard item;
  final OMDBRepository omdbRepository = OMDBRepository();
  final UserRepository userRepository = UserRepository();

  MovieDetail({@required this.movieId, @required this.item});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder<Movie>(
          future: omdbRepository.searchMovieById(id: movieId),
          builder: (BuildContext context, AsyncSnapshot<Movie> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
              case ConnectionState.done:
                if (snapshot.hasError)
                  return SafeArea(child: Text('Error: ${snapshot.error}'));
                if (snapshot.hasData) {
                  Movie movie = snapshot.data;
                  return ListView(
                    physics: ClampingScrollPhysics(),
                    padding: EdgeInsets.only(bottom: 30),
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.5,
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: ColorsConstants.shadow
                                              .withOpacity(.3),
                                          blurRadius:
                                              MediaQuery.of(context).size.width)
                                    ],
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(
                                            MediaQuery.of(context).size.width /
                                                2.5),
                                        bottomRight: Radius.circular(
                                            MediaQuery.of(context).size.width /
                                                2.5)),
                                    image: DecorationImage(
                                        image: NetworkImage(movie.posterUrl),
                                        fit: BoxFit.fitWidth)),
                              ),
                              Positioned(
                                top: 10 + MediaQuery.of(context).padding.top,
                                left: 6,
                                child: IconButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  icon: SvgPicture.asset(
                                    'assets/return.svg',
                                    alignment: Alignment(0, 0),
                                    height: 30,
                                    width: 30,
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 10 + MediaQuery.of(context).padding.top,
                                right: 6,
                                child: IconButton(
                                  onPressed: () async {
                                    await userRepository.addOrRemoveFavorite(
                                        favorite: Favorite(
                                            type: 'movie',
                                            searchItemCard: item));

                                    SnackBar snackBar = SnackBar(
                                      content:
                                          Text('Favorite successfully added'),
                                    );
                                    Scaffold.of(context).showSnackBar(snackBar);
                                  },
                                  icon: SvgPicture.asset(
                                    'assets/heart.svg',
                                    alignment: Alignment(0, 0),
                                    height: 30,
                                    width: 30,
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text(
                                  movie.title,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: ColorsConstants.red,
                                      fontSize: 23,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  '${movie.released} - ${movie.production}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SvgPicture.asset('assets/star.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      '${movie.rating}/100 (${movie.voteNumber} votes)',
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/hour.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      movie.runtime,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/eye.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      movie.rated,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/cup.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      movie.awards,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/folder.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      movie.genre,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    SvgPicture.asset('assets/website.svg'),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      movie.website,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Text(
                                  movie.plot,
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            height: 100,
                            child: ListView.builder(
                              itemCount: movie.actors.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                          child: SvgPicture.asset(
                                              'assets/person.svg')),
                                      Text(movie.actors[index])
                                    ],
                                  ),
                                );
                              },
                              scrollDirection: Axis.horizontal,
                            ),
                          )
                        ],
                      ),
                    ],
                  );
                }
                return Container();
            }
          },
        ),
      ),
    );
  }
}
