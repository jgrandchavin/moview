import 'package:flutter/material.dart';
import 'package:moview_flutter/src/constants/colors_constants.dart';
import 'package:moview_flutter/src/models/series.dart';

class SeasonDetail extends StatelessWidget {
  final Season season;

  SeasonDetail({this.season});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorsConstants.red,
      ),
      body: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: ListView.builder(
            padding: EdgeInsets.only(top: 30),
            itemCount: season.episodes.length,
            itemBuilder: (context, index) {
              return Text('${index + 1} ${season.episodes[index].title}');
            },
          )),
    );
  }
}
