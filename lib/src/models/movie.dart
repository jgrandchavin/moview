class Movie {
  String id;
  String title;
  String released;
  String rated;
  String voteNumber;
  String runtime;
  String genre;
  String production;
  List<String> actors;
  String posterUrl;
  String plot;
  String awards;
  String rating;
  String website;

  Movie.fromJson(Map<String, dynamic> json)
      : this.id = json['imdbID'],
        this.title = json['Title'],
        this.released = json['Released'],
        this.rating = json['imdbRating'],
        this.voteNumber = json['imdbVotes'],
        this.posterUrl =
            'http://img.omdbapi.com/?i=${json['imdbID']}&h=600&apikey=75522b56',
        this.rated = json['Rated'],
        this.runtime = json['Runtime'],
        this.genre = json['Genre'],
        this.production = json['Production'],
        this.actors = List.from(json['Actors'].split(', ')),
        this.plot = json['Plot'],
        this.awards = json['Awards'],
        this.website = json['Website'];
}
