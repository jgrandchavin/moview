class SearchItemCard extends Object {
  final String id;
  String title;
  String releasedYear;
  String posterUrl;
  String type;

  SearchItemCard({this.id, this.title, this.releasedYear, this.posterUrl});

  SearchItemCard.fromJson(Map<String, dynamic> json)
      : id = json['imdbID'],
        title = json['Title'],
        type = json['Type'],
        releasedYear = json['Year'],
        posterUrl = json['Poster'];

  toJson() {
    return {
      'imdbID': this.id,
      'Title': this.title,
      'Type': this.type,
      'Year': this.releasedYear,
      'Poster': this.posterUrl
    };
  }
}
