import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:moview_flutter/src/models/search_item_card.dart';

class Favorite {
  String id;
  SearchItemCard searchItemCard;
  String type;

  Favorite({this.id, this.searchItemCard, this.type});

  toJson() {
    return {'searchItemCard': this.searchItemCard.toJson(), 'type': this.type};
  }

  Favorite.fromSnapshot(DocumentSnapshot snapshot)
      : this.id = snapshot.documentID,
        this.type = snapshot.data['type'],
        this.searchItemCard = SearchItemCard(
            id: snapshot.data['searchItemCard']['imdbID'],
            posterUrl: snapshot.data['searchItemCard']['Poster'],
            releasedYear: snapshot.data['searchItemCard']['Year'],
            title: snapshot.data['searchItemCard']['Title']);
}
