import 'package:moview_flutter/src/models/favorite.dart';

class User {
  String id;
  String email;
  String firstName;
  String lastName;
  List<Favorite> favorites;

  User({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
  });

  toJson() {
    return {
      'email': this.email,
      'firstName': this.firstName,
      'lastName': this.lastName,
    };
  }
}
