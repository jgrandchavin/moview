import 'package:moview_flutter/src/models/search_item_card.dart';

class SearchResults extends Object {
  int totalResults;
  List<SearchItemCard> results;

  SearchResults({this.totalResults, this.results});

  SearchResults.fromJson(Map<String, dynamic> json)
      : totalResults = int.parse(json['totalResults']),
        results = (json['Search'] as List)
            .map((json) => SearchItemCard.fromJson(json))
            .toList();
}
