class Series {
  String id;
  String title;
  String released;
  String rated;
  String voteNumber;
  String runtime;
  String genre;
  String production;
  List<String> actors;
  String posterUrl;
  String plot;
  String awards;
  String rating;
  String website;
  List<Season> seasons;

  Series.fromJson(Map<String, dynamic> json)
      : this.id = json['imdbID'],
        this.title = json['Title'],
        this.released = json['Released'],
        this.rating = json['imdbRating'],
        this.voteNumber = json['imdbVotes'],
        this.posterUrl =
            'http://img.omdbapi.com/?i=${json['imdbID']}&h=600&apikey=75522b56',
        this.rated = json['Rated'],
        this.runtime = json['Runtime'],
        this.genre = json['Genre'],
        this.production = json['Production'],
        this.actors = List.from(json['Actors'].split(', ')),
        this.plot = json['Plot'],
        this.awards = json['Awards'],
        this.website = json['Website'];
}

class Season {
  int number;
  List<Episode> episodes;

  Season({this.number, this.episodes});
}

class Episode {
  int number;
  String title;
  String released;
  String runtime;
  String plot;
  String rating;
  String voteNumber;

  Episode.fromJson(dynamic json)
      : this.title = json['Title'],
        this.released = json['Released'],
        this.runtime = json['Runtime'],
        this.plot = json['Plot'],
        this.rating = json['imdbRating'],
        this.voteNumber = json['imdbVotes'];
}
