import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/authentication/register_bloc.dart';
import 'package:moview_flutter/src/repositories/user_repository.dart';
import 'package:moview_flutter/src/ui/authentication/register_page.dart';
import 'package:moview_flutter/src/ui/main_screen.dart';
import 'package:moview_flutter/src/ui/splash_screen.dart';
import 'package:rxdart/rxdart.dart';

class StartFlowBloc extends Bloc {
  // Repositories
  UserRepository _userRepository = UserRepository();

  // Streams
  ReplaySubject<Widget> _viewController = ReplaySubject<Widget>();
  Sink<Widget> get _inView => _viewController.sink;
  Stream<Widget> get outView => _viewController.stream;

  StartFlowBloc() {
    _inView.add(SplashScreen());
    Future.delayed(Duration(seconds: 2)).then((_) {
      _userRepository.isUserConnected().then((bool isUserConnected) {
        if (isUserConnected) {
          _inView.add(MainScreen());
        } else {
          _inView.add(BlocProvider<RegisterBloc>(
              creator: (_context, _bag) => RegisterBloc(),
              child: RegisterPage()));
        }
        this.dispose();
      });
    });
  }

  @override
  void dispose() {
    _viewController?.close();
  }
}
