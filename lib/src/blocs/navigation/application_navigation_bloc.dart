import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/favorite_bloc.dart';
import 'package:moview_flutter/src/ui/account/account_page.dart';
import 'package:moview_flutter/src/ui/home/home_page.dart';
import 'package:moview_flutter/src/ui/map/map_page.dart';
import 'package:moview_flutter/src/ui/search/search_page.dart';
import 'package:rxdart/rxdart.dart';

class ApplicationNavigationBloc extends Bloc {
  // View Stream
  ReplaySubject<Widget> _viewController = ReplaySubject<Widget>();
  Sink<Widget> get _inView => _viewController.sink;
  Stream<Widget> get outView => _viewController.stream;

  // TabIndex Stream
  ReplaySubject<int> _tabIndexController = ReplaySubject<int>();
  Sink<int> get _inIndex => _tabIndexController.sink;
  Stream<int> get outIndex => _tabIndexController.stream;

  ApplicationNavigationBloc() {
    _inIndex.add(0);
    _inView.add(HomePage());

    outIndex.listen((int index) {
      print(index);
      switch (index) {
        case 0:
          _inView.add(HomePage());
          break;
        case 1:
          _inView.add(SearchPage());
          break;
        case 2:
          _inView.add(MapPage());
          break;
        case 3:
          _inView.add(BlocProvider<FavoriteBloc>(
            creator: (BuildContext context, BlocCreatorBag bag) {
              return FavoriteBloc();
            },
            child: AccountPage(),
          ));
          break;
        default:
      }
    });
  }

  /// Add [newIndex] value to tabIndex stream.
  changeIndex(int newIndex) {
    _inIndex.add(newIndex);
  }

  @override
  void dispose() {
    _viewController.close();
    _tabIndexController.close();
  }
}
