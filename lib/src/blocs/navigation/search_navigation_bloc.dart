import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/search_movie_bloc.dart';
import 'package:moview_flutter/src/blocs/search_serie_bloc.dart';
import 'package:moview_flutter/src/ui/search/pages/movies_list_page.dart';
import 'package:moview_flutter/src/ui/search/pages/series_list_page.dart';
import 'package:rxdart/rxdart.dart';

class SearchNavigationBloc extends Bloc {
  // View Stream
  PublishSubject<Widget> _viewController = PublishSubject<Widget>();
  Sink<Widget> get _inView => _viewController.sink;
  Stream<Widget> get outView => _viewController.stream;

  // ViewKey Stream
  PublishSubject<String> _viewKeyController = PublishSubject<String>();
  Sink<String> get _inViewKey => _viewKeyController.sink;
  Stream<String> get outViewKey => _viewKeyController.stream;

  SearchNavigationBloc();

  changeView(String viewKey) {
    switch (viewKey) {
      case 'series':
        _inView.add(BlocProvider<SearchSerieBloc>(
          child: SeriesListPage(),
          creator: (BuildContext context, BlocCreatorBag bag) =>
              SearchSerieBloc('series'),
        ));
        _inViewKey.add('series');
        break;
      case 'movies':
        _inView.add(BlocProvider<SearchMovieBloc>(
          child: MoviesListPage(),
          creator: (BuildContext context, BlocCreatorBag bag) =>
              SearchMovieBloc(),
        ));
        _inViewKey.add('movies');
        break;
      default:
    }
  }

  @override
  void dispose() {
    _viewKeyController.close();
    _viewController.close();
  }
}
