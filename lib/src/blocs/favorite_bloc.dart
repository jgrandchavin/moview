import 'package:bloc_provider/bloc_provider.dart';
import 'package:moview_flutter/src/models/favorite.dart';
import 'package:moview_flutter/src/repositories/user_repository.dart';
import 'package:rxdart/rxdart.dart';

class FavoriteBloc extends Bloc {
  UserRepository userRepository = UserRepository();

  // Favorite Stream
  PublishSubject<List<Favorite>> _favoriteController =
      PublishSubject<List<Favorite>>();
  Sink<List<Favorite>> get _inItemsList => _favoriteController.sink;
  Stream<List<Favorite>> get outItemsList => _favoriteController.stream;

  FavoriteBloc() {
    userRepository.getAllFavorite().then((List<Favorite> res) {
      _inItemsList.add(res);
    });
  }

  @override
  void dispose() {
    _favoriteController?.close();
  }
}
