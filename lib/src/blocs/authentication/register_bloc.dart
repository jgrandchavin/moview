import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/navigation/application_navigation_bloc.dart';
import 'package:moview_flutter/src/models/user.dart';
import 'package:moview_flutter/src/repositories/user_repository.dart';
import 'package:moview_flutter/src/ui/main_screen.dart';
import 'package:moview_flutter/src/validators/common_text_validator.dart';
import 'package:moview_flutter/src/validators/email_validator.dart';
import 'package:moview_flutter/src/validators/password_validator.dart';
import 'package:rxdart/rxdart.dart';

class RegisterBloc extends Bloc
    with
        EmailValidator,
        EmailFreeValidator,
        PasswordValidator,
        CommonTextValidator {
  // Repositories
  final UserRepository _userRepository = UserRepository();

  // Email
  final BehaviorSubject<String> _emailController = BehaviorSubject<String>();
  Function(String) get inEmail => _emailController.sink.add;
  Stream<String> get outEmail =>
      _emailController.stream.transform(validateEmail).transform(isEmailFree);

  // Password
  final BehaviorSubject<String> _passwordController = BehaviorSubject<String>();
  Function(String) get inPassword => _passwordController.sink.add;
  Stream<String> get outPassword =>
      _passwordController.stream.transform(validatePassword);

  // Confirm password
  final BehaviorSubject<String> _confirmPasswordController =
      BehaviorSubject<String>();
  Function(String) get inConfirmPassword => _confirmPasswordController.sink.add;
  Stream<String> get outConfirmPassword => _confirmPasswordController.stream
          .transform(validatePassword)
          .doOnData((String c) {
        if (0 != _passwordController.value.compareTo(c)) {
          _confirmPasswordController
              .addError("Les mots de passes ne correspondent pas");
        }
      });

  // Is loading ?
  final BehaviorSubject<bool> _isLoadingController = BehaviorSubject<bool>();
  Function(bool) get inIsLoading => _isLoadingController.sink.add;
  Stream<bool> get outIsLoading => _isLoadingController.stream;

  // Is register form valid
  Stream<bool> get isRegisterFormValid => Observable.combineLatest3(
      outEmail,
      outPassword,
      outConfirmPassword,
      (email, password, confirmPassword) => true);

  /// Register user
  registerUser(BuildContext context) async {
    inIsLoading(true);
    User newUser = User(
      email: _emailController.value,
    );
    try {
      await _userRepository.registerUser(
          newUser: newUser, password: _passwordController.value);
      inIsLoading(false);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) {
        return BlocProvider<ApplicationNavigationBloc>(
            creator: (_context, _bag) => ApplicationNavigationBloc(),
            child: MainScreen());
      }), (Route<dynamic> route) => false);
    } catch (e) {
      SnackBar snackBar = SnackBar(
        content: Text('Erreur'),
      );
      Scaffold.of(context).showSnackBar(snackBar);
      inIsLoading(false);
    }

    this.dispose();
  }

  @override
  void dispose() {
    _emailController?.close();
    _passwordController?.close();
    _confirmPasswordController?.close();
    _isLoadingController?.close();
  }
}
