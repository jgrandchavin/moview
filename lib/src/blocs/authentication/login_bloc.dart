import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/navigation/application_navigation_bloc.dart';
import 'package:moview_flutter/src/repositories/user_repository.dart';
import 'package:moview_flutter/src/ui/main_screen.dart';
import 'package:moview_flutter/src/validators/common_text_validator.dart';
import 'package:moview_flutter/src/validators/email_validator.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends Bloc with EmailValidator, CommonTextValidator {
  // Repositories
  final UserRepository _userRepository = UserRepository();

  // Email
  final BehaviorSubject<String> _emailController = BehaviorSubject<String>();
  Function(String) get inEmail => _emailController.sink.add;
  Stream<String> get outEmail =>
      _emailController.stream.transform(validateEmail);

  // Password
  final BehaviorSubject<String> _passwordController = BehaviorSubject<String>();
  Function(String) get inPassword => _passwordController.sink.add;
  Stream<String> get outPassword =>
      _passwordController.stream.transform(validateText);

  // Loading
  final BehaviorSubject<bool> _isLoadingController = BehaviorSubject<bool>();
  Function(bool) get _inIsLoading => _isLoadingController.sink.add;
  Stream<bool> get outIsLoading => _isLoadingController.stream;

  // Is login form valid
  Stream<bool> get isLoginFormValid => Observable.combineLatest2(
      outEmail, outPassword, (email, password) => true);

  LoginBloc() {
    _inIsLoading(false);
  }

  /// Login user
  loginUser(BuildContext context) async {
    _inIsLoading(true);
    try {
      await _userRepository.signInUser(
          email: _emailController.value, password: _passwordController.value);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => BlocProvider<ApplicationNavigationBloc>(
                  creator: (_context, _bag) => ApplicationNavigationBloc(),
                  child: MainScreen())),
          (Route<dynamic> route) => false);

      _inIsLoading(false);
    } catch (e) {
      SnackBar snackBar = SnackBar(
        content: Text('Erreur'),
      );
      Scaffold.of(context).showSnackBar(snackBar);
      _inIsLoading(false);
    }
  }

  @override
  void dispose() {
    _emailController?.close();
    _passwordController?.close();
    _isLoadingController?.close();
  }
}
