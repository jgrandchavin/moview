import 'package:bloc_provider/bloc_provider.dart';
import 'package:moview_flutter/src/models/search_results.dart';
import 'package:moview_flutter/src/repositories/omdb_repository.dart';
import 'package:rxdart/rxdart.dart';

class SearchMovieBloc implements Bloc {
  final OMDBRepository omdbRepository = OMDBRepository();

  // MovieSearch Stream
  PublishSubject<SearchResults> _itemsSearcherController =
      PublishSubject<SearchResults>();
  Sink<SearchResults> get _inItemsList => _itemsSearcherController.sink;
  Stream<SearchResults> get outItemsList => _itemsSearcherController.stream;

  // SearchedTitle Stream
  PublishSubject<String> _searchedTitleController = PublishSubject<String>();
  Sink<String> get inSearchedTitle => _searchedTitleController.sink;
  Stream<String> get _outSearchedTitle => _searchedTitleController.stream;

  SearchMovieBloc() {
    _outSearchedTitle.listen((String value) {
      _searchedMovies(value);
    });
  }

  /// Search movies by [searchedName]
  _searchedMovies(String searchedName) async {
    SearchResults searchResults = await omdbRepository.searchItemsByName(
        searchedName: searchedName, searchType: 'movie');
    _inItemsList.add(searchResults);
  }

  @override
  void dispose() {
    _searchedTitleController.close();
    _itemsSearcherController.close();
  }
}
