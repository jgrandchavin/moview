import 'package:bloc_provider/bloc_provider.dart';
import 'package:moview_flutter/src/models/search_results.dart';
import 'package:moview_flutter/src/repositories/omdb_repository.dart';
import 'package:rxdart/rxdart.dart';

class SearchSerieBloc implements Bloc {
  final OMDBRepository omdbRepository = OMDBRepository();

  // MovieSearch Stream
  PublishSubject<SearchResults> _itemsSearcherController =
      PublishSubject<SearchResults>();
  Sink<SearchResults> get _inItemsList => _itemsSearcherController.sink;
  Stream<SearchResults> get outItemsList => _itemsSearcherController.stream;

  // SearchedTitle Stream
  PublishSubject<String> _searchedTitleController = PublishSubject<String>();
  Sink<String> get inSearchedTitle => _searchedTitleController.sink;
  Stream<String> get _outSearchedTitle => _searchedTitleController.stream;

  SearchSerieBloc(String searchType) {
    _outSearchedTitle.listen((String value) {
      _searchedSeries(value);
    });
  }

  /// Search series by [searchedName]
  _searchedSeries(String searchedName) async {
    SearchResults searchResults = await omdbRepository.searchItemsByName(
        searchedName: searchedName, searchType: 'series');
    _inItemsList.add(searchResults);
  }

  @override
  void dispose() {
    _searchedTitleController.close();
    _itemsSearcherController.close();
  }
}
