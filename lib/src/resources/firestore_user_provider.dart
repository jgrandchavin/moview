import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:moview_flutter/src/models/favorite.dart';
import 'package:moview_flutter/src/models/user.dart';

class FirestoreUserProvider {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  Firestore firebaseDatabase = Firestore.instance;

  CollectionReference _usersDb;

  FirestoreUserProvider() {
    firebaseDatabase.enablePersistence(true);
    _usersDb = firebaseDatabase.collection('users');
  }

  /// Register [newUser] with [password]
  Future<void> registerUser({User newUser, String password}) async {
    await _firebaseAuth.createUserWithEmailAndPassword(
        email: newUser.email, password: password);

    await _usersDb.document(newUser.email).setData(newUser.toJson());
  }

  /// Check if user is connected
  Future<bool> isUserConnected() async {
    FirebaseUser _firebaseUser = await _firebaseAuth.currentUser();
    return _firebaseUser != null;
  }

  /// Sign out current user
  Future<void> signOutUser() async {
    await _firebaseAuth.signOut();
  }

  /// Check if [email] is free
  Future<bool> isEmailFree({String email}) async {
    QuerySnapshot query = await _usersDb.getDocuments();

    bool response = true;

    Future.wait(query.documents.map((DocumentSnapshot snap) async {
      if (snap.data['email'] == email) {
        response = false;
      }
    }));

    return response;
  }

  /// Sign in user with [email] & [password]
  Future<FirebaseUser> signInUser({String email, String password}) async {
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
    return user;
  }

  Future<void> addOrRemoveFavorite({Favorite favorite}) async {
    FirebaseUser user = await _firebaseAuth.currentUser();

    CollectionReference favoriteColl =
        _usersDb.document(user.email).collection('favorites');
    await favoriteColl.document().setData(favorite.toJson());
  }

  Future<List<Favorite>> getAllFavorite() async {
    FirebaseUser user = await _firebaseAuth.currentUser();

    QuerySnapshot querySnapshot = await _usersDb
        .document(user.email)
        .collection('favorites')
        .getDocuments();

    print(querySnapshot.documents.length);
    return querySnapshot.documents.map((DocumentSnapshot snap) {
      return Favorite.fromSnapshot(snap);
    }).toList();
  }
}
