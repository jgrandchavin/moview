import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:moview_flutter/src/models/movie.dart';
import 'package:moview_flutter/src/models/search_results.dart';
import 'package:moview_flutter/src/models/series.dart';

class OMDBApiProvider {
  Client client = Client();
  final apiKey = '75522b56';

  /// Search [searchType] by [searchedName].
  ///
  /// Throw an [Exception] if Http response is not 200.
  Future<SearchResults> searchItemsByName(
      {String searchedName, String searchType}) async {
    final response = await client.get(
        'http://www.omdbapi.com/?apikey=75522b56&type=$searchType&s=$searchedName');

    if (response.statusCode == 200) {
      if (json.decode(response.body)['Response'] == 'True') {
        return SearchResults.fromJson(json.decode(response.body));
      } else {
        return SearchResults(results: [], totalResults: 0);
      }
    } else {
      throw Exception('Failed to search items');
    }
  }

  /// Search movie by [id]
  Future<Movie> searchMovieById({String id}) async {
    final response = await client
        .get('http://www.omdbapi.com/?apikey=75522b56&&i=$id&plot=full');

    if (response.statusCode == 200) {
      if (json.decode(response.body)['Response'] == 'True') {
        return Movie.fromJson(json.decode(response.body));
      } else {
        throw Exception('Failed to search movie');
      }
    } else {
      throw Exception('Failed to search movie');
    }
  }

  /// Get series by [id]
  Future<Series> getSeriesById({String id}) async {
    final response = await client
        .get('http://www.omdbapi.com/?apikey=75522b56&&i=$id&plot=full');

    if (response.statusCode == 200) {
      if (json.decode(response.body)['Response'] == 'True') {
        Series series = Series.fromJson(json.decode(response.body));
        List<Season> s = [];
        int totalSeason = int.parse(json.decode(response.body)['totalSeasons']);
        for (int i = 1; i < totalSeason; i++) {
          Season season = Season(number: i);
          final response2 = await client
              .get('http://www.omdbapi.com/?apikey=75522b56&i=$id&season=$i');
          List<Episode> episodes =
              List.from(json.decode(response2.body)['Episodes']).map((episode) {
            return Episode.fromJson(episode);
          }).toList();
          season.episodes = episodes;
          s.add(season);
          series.seasons = s;
        }
        return series;
      } else {
        throw Exception('Failed to search series');
      }
    } else {
      throw Exception('Failed to search series');
    }
  }
}
