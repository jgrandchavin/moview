import 'package:flutter/material.dart';

class ColorsConstants {
  static const black = Color(0xff1D1E1F);
  static const red = Color(0xffFF5252);
  static const shadow = Color(0xff000000);
  static const lightGrey = Color(0xffDADADA);
}
