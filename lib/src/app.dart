import 'package:bloc_provider/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:moview_flutter/src/blocs/flows/start_flow_bloc.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Averta'),
      home: BlocProvider<StartFlowBloc>(
          creator: (_context, _bag) => StartFlowBloc(), child: CurrentView()),
    );
  }
}

class CurrentView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final StartFlowBloc navBloc = BlocProvider.of<StartFlowBloc>(context);
    return StreamBuilder<Widget>(
      stream: navBloc.outView,
      builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
        if (snapshot.hasData) {
          return snapshot.data;
        }
        return Container();
      },
    );
  }
}
